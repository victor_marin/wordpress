<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'demofili_victor');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'admin');

/** MySQL hostname */
define('DB_HOST', 'demo.filipnet.ro');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'B9]FSiEuOZB8WF{4|d4+0E+BN.49tG.}IHfxiFCE$}x>&;FY]>f4T=|z{_~y0-|P');
define('SECURE_AUTH_KEY',  ' `,ErQ-^E=HQth[n4D{a#/>y^.uxn{OFnCtpY%KA.VP>*h5B<cax-L$8NWOac*4?');
define('LOGGED_IN_KEY',    '|,&&lN%]8<_%$?*mM?wV7F#HRl&gDismoZjtRWB|-GeSP~hWcZ<>z86lc,h!4rpQ');
define('NONCE_KEY',        'b[b)t(?jGmnGa-,XA=Sh:?=kFC1[6_lh/ew-SCd {@j,bG/x2r-<|%>F+KTCn$0~');
define('AUTH_SALT',        'hi=,*_**bXte)wbxaz4|6z%Kt)zwl!-MhrDYfA@$hW5osq%|@5Numr;jz-TdHQ1k');
define('SECURE_AUTH_SALT', ' U&fgc2B>9#OXf;b=Ps}?FV=XI^F&tpNrmU~^%|UtS5|oBiC0p7<j$kz,))`HBb4');
define('LOGGED_IN_SALT',   '}/^rZ:i>CP=Ez~xp IDE8/q=^=K>@bxtz4JvhIjCk7nG2vCWU9%<II9_`icDo0)C');
define('NONCE_SALT',       '0.<Q6`iB,_tMydpW_*6Q2#s|%U3*pTnelmD^]&nxqa&b VG3Bx-{eji|t+kT_CF>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
